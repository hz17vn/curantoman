package com.cryptobuddy.ryanbridges.cryptobuddy.service.billing;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.service.firebase.NotificationFactory;
import com.cryptobuddy.ryanbridges.cryptobuddy.service.firebase.NotificationProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BillingService extends Service implements PurchasesUpdatedListener {

    private BillingClient mBillingClient;
    private boolean mIsServiceConnected;
    ArrayList<HashMap<String, String>> mPendingMessages;

    @Override
    public void onCreate() {
        Log.d("Curantoman-", "Curantoman- OnCreate");
        super.onCreate();
        mPendingMessages = new ArrayList<>();
        mBillingClient = BillingClient.newBuilder(this).setListener(this).build();

    }


    public boolean isVIP() {
        Log.d("Curantoman-", "Curantoman- queryPurchases");
        // If there are subscriptions supported, we add subscription rows as well
        if (areSubscriptionsSupported()) {
            Purchase.PurchasesResult subscriptionResult
                    = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
            if (subscriptionResult.getResponseCode() == BillingClient.BillingResponse.OK) {
                if (subscriptionResult.getPurchasesList() == null) return false;
                return subscriptionResult.getPurchasesList().size() > 0;
            } else {
            }
        }
        return false;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Curantoman-", "Curantoman- onBind");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        Log.d("Curantoman-", "Curantoman- onStartCommand");
        if (intent != null && intent.hasExtra("map")) {
            HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("map");
            if (mIsServiceConnected == false) {
                mPendingMessages.add(hashMap);
                startServiceConnection();
            } else {
                if (hashMap.get("user_type") != null && hashMap.get("user_type").equals("paid") && !isVIP())
                    pushNotify(hashMap, true);
                else pushNotify(hashMap, false);
            }
        }
        return START_STICKY;
    }

    private void pushNotify(HashMap<String, String> hashMap, boolean isHide) {
        NotificationProvider notificationProvider = NotificationFactory.create(getApplicationContext(), hashMap, isHide);
        if (notificationProvider != null) {
            notificationProvider.pushNotify();
        }

    }

    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBillingClient != null && mBillingClient.isReady()) {
            mBillingClient.endConnection();
            mBillingClient = null;
        }
        mIsServiceConnected = false;
    }

    public void onReady() {
        boolean isVIP = isVIP();
        if (mPendingMessages != null && mPendingMessages.size() > 0)
            for (HashMap<String, String> hasmap : mPendingMessages) {
                if (hasmap.get("user_type") != null && hasmap.get("user_type").equals("paid") && !isVIP)
                    pushNotify(hasmap, true);
                else pushNotify(hasmap, false);

                mPendingMessages.remove(hasmap);
            }
    }

    public void startServiceConnection() {
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
                Log.d("Curantoman-", "Curantoman- onBillingSetupFinished");

                if (billingResponseCode == BillingClient.BillingResponse.OK) {
                    mIsServiceConnected = true;
                    onReady();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                mIsServiceConnected = false;
            }
        });
    }


    public boolean areSubscriptionsSupported() {
        int responseCode = mBillingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS);
        if (responseCode != BillingClient.BillingResponse.OK) {
        }
        return responseCode == BillingClient.BillingResponse.OK;
    }
}
