package com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model;

public class AccessToken {
    private String access_token;

    public String getAccessToken() {
        return access_token;
    }
}
