package com.cryptobuddy.ryanbridges.cryptobuddy.helper;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.MainApplication;
import com.cryptobuddy.ryanbridges.cryptobuddy.currencylist.CurrencyListTabsActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.service.app.AppService;

public final class AppHelper {
    public static void restartApp(Context context) {
        Intent intent = new Intent(context, CurrencyListTabsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void loadAccessToken(Context context) {
        Intent intent = new Intent(context, AppService.class);
        intent.setAction(AppService.ACTION_LOAD_ACCESS_TOKEN);
        context.startService(intent);
    }

    public static void changeLanguage(Context context) {
        Intent intent = new Intent(context, AppService.class);
        intent.setAction(AppService.ACTION_CHANGE_LANGUAGE);
        context.startService(intent);
    }

    public static String getContryCode(Context context) {
        Resources res = context.getResources();
        String[] codes = res.getStringArray(R.array.country_codes);
        return codes[0]+codes[5]+codes[1]+codes[4]+codes[7]+codes[3]+codes[2]+codes[6];
    }
}
