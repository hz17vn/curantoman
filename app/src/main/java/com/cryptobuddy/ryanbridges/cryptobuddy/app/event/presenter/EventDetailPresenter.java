package com.cryptobuddy.ryanbridges.cryptobuddy.app.event.presenter;

import com.cryptobuddy.ryanbridges.cryptobuddy.app.BasePresenter;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.view.EventDetailView;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.EventInteractor;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.OnResponseListener;

public class EventDetailPresenter extends BasePresenter<EventDetailView> {
    private final EventInteractor mInteractor;

    public EventDetailPresenter(EventInteractor eventInteractor) {
        mInteractor = eventInteractor;
    }

    public void loadEvent(String authorization, int eventId) {
        if(eventId <= 0) {
            return;
        }
        mInteractor.loadEvent(authorization, eventId, new OnResponseListener<EventItem>() {
            @Override
            public void onSuccess(EventItem data) {
                EventDetailView view = getView();
                if(view == null) {
                    return;
                }
                view.showEvents(data);
            }

            @Override
            public void onFailure(ErrorType error) {
                if(getView() == null) {
                    return;
                }
                getView().onShowEventsFailure(error);
            }
        });
    }
}
