/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cryptobuddy.ryanbridges.cryptobuddy.billing;

import android.graphics.Color;

import com.android.billingclient.api.BillingClient.SkuType;
import com.cryptobuddy.ryanbridges.cryptobuddy.R;

import java.util.ArrayList;

/**
 * Handles Ui specific to "yearly gas" - subscription row
 */
public class Gold6MonthsDelegate extends UiManagingDelegate {
    public static final String SKU_ID = "6months";

    public Gold6MonthsDelegate(BillingProvider billingProvider) {
        super(billingProvider);
    }

    @Override
    public @SkuType
    String getType() {
        return SkuType.SUBS;
    }

    @Override
    public void onBindViewHolder(SkuRowData data, RowViewHolder holder) {
        super.onBindViewHolder(data, holder);
        if (mBillingProvider.isGold6MonthSubscribed()) {
            holder.button.setText(R.string.button_own);
            holder.cardView.setCardBackgroundColor(Color.parseColor("#BEBEBE"));
            holder.cardView.setEnabled(false);

        } else {
            int textId = mBillingProvider.getCurrentSKU() != null
                    ? R.string.button_change : R.string.button_buy;
            holder.button.setText(textId);
        }
        holder.skuIcon.setImageResource(R.drawable.vip3);
    }

    @Override
    public void onButtonClicked(SkuRowData data) {
        if (data != null) {
            if (mBillingProvider.getCurrentSKU() != null && !mBillingProvider.getCurrentSKU().equals(SKU_ID)) {
                // If we already subscribed to yearly gas, launch replace flow
                ArrayList<String> currentSubscriptionSku = new ArrayList<>();
                currentSubscriptionSku.add(mBillingProvider.getCurrentSKU());
                mBillingProvider.getBillingManager().initiatePurchaseFlow(data.getSku(),
                        currentSubscriptionSku, data.getSkuType());
            } else {
                mBillingProvider.getBillingManager().initiatePurchaseFlow(data.getSku(),
                        data.getSkuType());
            }
        }
    }
}
