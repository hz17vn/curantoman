package com.cryptobuddy.ryanbridges.cryptobuddy.helper;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class util {

    public static String getStringByLocal(Context context, int id) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(Locale.getDefault());
        return context.createConfigurationContext(configuration).getResources().getString(id);
    }
}
