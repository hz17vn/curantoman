package com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor;

import android.text.TextUtils;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.RestfulService;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.AccessToken;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.DataWrap;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventInteractor {

    public void loadEvents(String authorization, int page, final OnResponseListener<List<EventItem>> listener) {
        if(listener == null) {
            return;
        }
        Call<List<EventItem>> call = RestfulService.getInstance().loadEvents(authorization, page);
        call.enqueue(new Callback<List<EventItem>>() {
            @Override
            public void onResponse(Call<List<EventItem>> call, Response<List<EventItem>> response) {
                if(response.code() == 403 || response.code() == 401) {
                    listener.onFailure(new ErrorType(response.code(), response.message()));
                    return;
                }
                if(response.body() == null) {
                    listener.onSuccess(new ArrayList<EventItem>());
                    return;
                }
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<EventItem>> call, Throwable t) {
                listener.onFailure(new ErrorType(t.getMessage()));
            }
        });
    }

    public void loadEvent(String authorization, int eventId, final OnResponseListener<EventItem> listener) {
        if(listener == null) {
            return;
        }
        Call<DataWrap<EventItem>> call = RestfulService.getInstance().loadEvent(authorization, eventId);
        call.enqueue(new Callback<DataWrap<EventItem>>() {
            @Override
            public void onResponse(Call<DataWrap<EventItem>> call, Response<DataWrap<EventItem>> response) {
                if(response.code() == 403 || response.code() == 401) {
                    listener.onFailure(new ErrorType(response.code(), response.message()));
                    return;
                }
                if(response.body() == null) {
                    listener.onSuccess(null);
                    return;
                }
                listener.onSuccess(response.body().getData());
            }

            @Override
            public void onFailure(Call<DataWrap<EventItem>> call, Throwable t) {
                listener.onFailure(new ErrorType(t.getMessage()));
            }
        });
    }

    public void loadAccessToken(String deviceTokenId, final OnResponseListener<String> listener) {
        if(listener == null) {
            return;
        }
        Call<AccessToken> call = RestfulService.getInstance().loadAccessToken(deviceTokenId);
        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                AccessToken dataResponse = response.body();
                if(dataResponse != null &&!TextUtils.isEmpty(dataResponse.getAccessToken())) {
                    listener.onSuccess(dataResponse.getAccessToken());
                }
            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                listener.onFailure(new ErrorType(t.getMessage()));
            }
        });
    }
}
