package com.cryptobuddy.ryanbridges.cryptobuddy.app.event;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.BaseRecyclerViewAdapter;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.BillingProvider;
import com.cryptobuddy.ryanbridges.cryptobuddy.currencylist.CurrencyListTabsActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.CheckedManager;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.ViewHelper;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.util;

import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;

public class EventRecycleAdapter extends BaseRecyclerViewAdapter<EventItem, EventRecycleAdapter.EventHolder> {


    private BillingProvider mBillingProvider;
    private WeakReference<CheckedManager> mDbRef;

    public EventRecycleAdapter(Context context) {
        super(context);
        mBillingProvider = (CurrencyListTabsActivity) context;
        mDbRef = new WeakReference<>(CheckedManager.getInstance(context));
    }

    @NonNull
    @Override
    public EventHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_event_item, parent, false);
        return new EventHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EventHolder holder, int position) {
        EventItem item = mData.get(position);
        holder.tvTitle.setText(item.getTitle());
        holder.tvCreatedTime.setText(ViewHelper.formatDateTime(new Date(item.getCreatedAt())));
        holder.tvSummary.setText(item.getDescription());
        if ("bitcoin".equals(item.getEventType())) {
            holder.imvCoin.setImageResource(R.drawable.bitgold);
        } else {
            holder.imvCoin.setImageResource(R.drawable.altcoin);
        }

        if ("paid".equals(item.getUser_type())) {
            if (mBillingProvider.getCurrentSKU() == null) {
                holder.tvTitle.setText(util.getStringByLocal(getContext(), R.string.title_free));
                holder.tvSummary.setText(util.getStringByLocal(getContext(), R.string.content_free));
                holder.tvCreatedTime.setText(ViewHelper.formatDateTime(new Date(item.getCreatedAt())));
                holder.imvCoin.setImageResource(R.drawable.vip);
                holder.imvVIP.setVisibility(View.INVISIBLE);
            }
        } else if ("all".equals(item.getUser_type())) {
            holder.imvVIP.setVisibility(View.VISIBLE);
        } else {
            holder.imvVIP.setVisibility(View.INVISIBLE);
        }




       /* if (!("paid".equals(item.getUser_type())) || mBillingProvider.getCurrentSKU() != null) {
            holder.tvTitle.setText(item.getTitle());
            holder.tvCreatedTime.setText(ViewHelper.formatDateTime(new Date(item.getCreatedAt())));
            if ("bitcoin".equals(item.getEventType())) {
                holder.imvCoin.setImageResource(R.drawable.bitgold);
            } else {
                holder.imvCoin.setImageResource(R.drawable.altcoin);
            }

            holder.tvSummary.setText(item.getDescription());
        }else if (("paid".equals(item.getUser_type())) && mBillingProvider.getCurrentSKU() != null){

        }
        else {
            holder.tvTitle.setText(util.getStringByLocal(getContext(), R.string.title_free));
            holder.tvSummary.setText(util.getStringByLocal(getContext(), R.string.content_free));
            holder.tvCreatedTime.setText(ViewHelper.formatDateTime(new Date(item.getCreatedAt())));
            holder.imvCoin.setImageResource(R.drawable.vip);
            holder.imvVIP.setVisibility(View.INVISIBLE);
        }
        */


        if (CheckedManager.getInstance(getContext()).isExist(String.valueOf(item.getId()), EventItem.TYPE)) {
            holder.tvTitle.setTypeface(null, Typeface.NORMAL);
        } else {
            holder.tvTitle.setTypeface(null, Typeface.BOLD);
        }
    }

    static class EventHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvSummary;
        TextView tvCreatedTime;
        ImageView imvCoin;
        ImageView imvVIP;

        EventHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvCreatedTime = itemView.findViewById(R.id.tv_date_time);
            imvCoin = itemView.findViewById(R.id.img_coin);
            tvSummary = itemView.findViewById(R.id.tv_summary);
            imvVIP = itemView.findViewById(R.id.img_vip);
        }
    }
}
