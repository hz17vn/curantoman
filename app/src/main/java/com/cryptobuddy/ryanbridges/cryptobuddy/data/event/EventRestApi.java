package com.cryptobuddy.ryanbridges.cryptobuddy.data.event;


import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.AccessToken;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.DataWrap;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.Language;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import okhttp3.OkHttpClient;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EventRestApi {
    String BASE_URL = "http://bigcoinsignal.com/";

    @GET("events/list")
    Call<List<EventItem>> loadEvents(@Header("Authorization") String authorization, @Query("page") int page);

    @GET("events/get/{eventId}/detail")
    Call<DataWrap<EventItem>> loadEvent(@Header("Authorization") String authorization, @Path("eventId") int eventId);

    @GET("auth/accessToken")
    Call<AccessToken> loadAccessToken(@Query("deviceTokenId") String deviceTokenId);

    @GET("http://translate.google.com/translate_a/single?client=at&dt=t&dt=ld&dt=qca&dt=rm&dt=bd&dj=1&sl=en&tl=vi&&hl=vi&ie=UTF-8&oe=UTF-8&inputm=2&otf=2&iid=1dd3b944-fa62-4b55-b330-74909a99969e")
    @Headers({"User-Agent: AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1"})
    Call<AccessToken> translate(@Query("q") String str3);

    class Factory {
        public static EventRestApi create() {
            return create(BASE_URL + Language.getDefaultLanguage() + "/api/frontend/");
        }

        public static EventRestApi create(String baseUrl) {
            OkHttpClient client = new OkHttpClient.Builder().build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(EventRestApi.class);
        }
    }
}
