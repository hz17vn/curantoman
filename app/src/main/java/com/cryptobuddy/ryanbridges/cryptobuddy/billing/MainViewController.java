/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.cryptobuddy.ryanbridges.cryptobuddy.billing;

import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import com.android.billingclient.api.BillingClient.BillingResponse;
import com.android.billingclient.api.Purchase;
import com.cryptobuddy.ryanbridges.cryptobuddy.currencylist.CurrencyListTabsActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.BillingManager.BillingUpdatesListener;


import java.io.UnsupportedEncodingException;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Handles control logic of the BaseGamePlayActivity
 */
public class MainViewController {
    private static final String TAG = "MainViewController";



    private final UpdateListener mUpdateListener;
    private CurrencyListTabsActivity mActivity;

    // Tracks if we currently own subscriptions SKUs
    private boolean mGoldMonthly;
    private boolean mGoldYearly;
    private boolean mGold3Months;
    private boolean mGold6Months;

    // Tracks if we currently own a premium car
    private boolean mIsPremium;

    // Current amount of gas in tank, in units
    private int mTank;

    public MainViewController(CurrencyListTabsActivity activity) {
        mUpdateListener = new UpdateListener();
        mActivity = activity;
    }



    public UpdateListener getUpdateListener() {
        return mUpdateListener;
    }


    public boolean isPremiumPurchased() {
        return mIsPremium;
    }

    public boolean isGoldMonthlySubscribed() {
        return mGoldMonthly;
    }
    public boolean isGold3MonthsSubscribed() {
        return mGold3Months;
    }
    public boolean isGold6MonthsSubscribed() {
        return mGold6Months;
    }

    public boolean isGoldYearlySubscribed() {
        return mGoldYearly;
    }


    /**
     * Handler to billing updates
     */
    private class UpdateListener implements BillingUpdatesListener {
        @Override
        public void onBillingClientSetupFinished() {
            if (mActivity != null)
            mActivity.onBillingManagerSetupFinished();
        }

        @Override
        public void onConsumeFinished(String token, @BillingResponse int result) {
            Log.d(TAG, "Consumption finished. Purchase token: " + token + ", result: " + result);

        }

        @Override
        public void onPurchasesUpdated(List<Purchase> purchaseList) {
            mGoldMonthly = false;
            mGoldYearly = false;
            mGold3Months = false;
            mGold6Months = false;

            String currentPurchase = "()$%^^";

            if (purchaseList != null && purchaseList.size() > 0) {
                Purchase lastPurchase = purchaseList.get(0);
                for (Purchase purchase : purchaseList) {
                    if (lastPurchase.getPurchaseTime() < purchase.getPurchaseTime()) lastPurchase = purchase;
                }


                currentPurchase = System.currentTimeMillis() + "#" + lastPurchase.getSku();

             //   for (Purchase purchase : purchaseList) {
                    switch (lastPurchase.getSku()) {
                        case PremiumDelegate.SKU_ID:
                            Log.d(TAG, "You are Premium! Congratulations!!!");


                            mIsPremium = true;
                            break;

                        case Gold3MonthsDelegate.SKU_ID:
                            mGold3Months = true;
                            break;
                        case Gold6MonthsDelegate.SKU_ID:
                            mGold6Months = true;
                            break;
                        case GoldMonthlyDelegate.SKU_ID:
                            mGoldMonthly = true;
                            break;
                        case GoldYearlyDelegate.SKU_ID:
                            mGoldYearly = true;
                            break;
                    }
              //  }
            }

            SharedPreferences.Editor editor = mActivity.getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
            editor.putString("name", endcode64(currentPurchase));
            editor.commit();

            if (mActivity != null) mActivity.showRefreshedUi();
        }
    }

    public String endcode64(String text){
// Sending side
        byte[] data = new byte[0];
        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String base64 = Base64.encodeToString(data, Base64.DEFAULT);
        return base64;
    }


}