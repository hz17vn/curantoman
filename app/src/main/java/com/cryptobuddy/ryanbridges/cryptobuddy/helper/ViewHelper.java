package com.cryptobuddy.ryanbridges.cryptobuddy.helper;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.EventDetailActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class ViewHelper {
    private static final String USER_AGENT_LOLLIPOP = "Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/43.0.2357.65 Mobile Safari/537.36";
    private static final String USER_AGENT_KITKAT = "Mozilla/5.0 (Linux; Android 4.4; Nexus 5 Build/_BuildID_) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/30.0.0.0 Mobile Safari/537.36";

    private static final String YY_MM_DD_HH_MM_FORMAT = "dd-MM-yyyy HH:mm";

    public static String formatDateTime(Date date) {
        return new SimpleDateFormat(YY_MM_DD_HH_MM_FORMAT, Locale.getDefault()).format(date);
    }

    public static void displayEventDetail(Context context, int id) {
        Intent intent = new Intent(context, EventDetailActivity.class);
        intent.putExtra("id", id);
        context.startActivity(intent);
    }

    public static void improveWebSetting(WebView webView) {
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setUserAgentString(USER_AGENT_KITKAT);
        } else {
            webSettings.setUserAgentString(USER_AGENT_LOLLIPOP);
        }
    }
}
