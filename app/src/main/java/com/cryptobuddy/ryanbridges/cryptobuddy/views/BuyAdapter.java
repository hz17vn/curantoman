package com.cryptobuddy.ryanbridges.cryptobuddy.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.SkuRowData;

import java.util.List;

/**
 * Created by sev_user on 4/5/2017.
 */


public class BuyAdapter extends ArrayAdapter {

    Context mContext;
    List<SkuRowData> mListSKU;
    int current_Purchased;

    public BuyAdapter(Context context, List<SkuRowData> inList, int index) {
        super(context, R.layout.price_item);
        mListSKU = inList;
        mContext = context;
        current_Purchased = index;
    }

    @Override
    public int getCount() {
        return mListSKU.size();
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = LayoutInflater.from(mContext).inflate(R.layout.price_item, parent, false);
        LinearLayout root = row.findViewById(R.id.layoutitem);
        TextView txtSlot =  row.findViewById(R.id.txtPackageName);
        TextView txtPriceSale = row.findViewById(R.id.txtpricesale);
        TextView txtSave = row.findViewById(R.id.txtsave);
        ImageView img =  row.findViewById(R.id.imgRow);


        txtPriceSale.setText(mListSKU.get(position).getPrice());

        switch (position) {
            case 0:
                txtSlot.setText(R.string.vip1_title);
                if (current_Purchased == position) {
                    // img.setImageResource(R.drawable.vip1_ok);
                    txtSlot.setTypeface(Typeface.DEFAULT_BOLD);
                    root.setBackgroundResource(R.drawable.layout_bg);
                } else if (current_Purchased != -1)
                    // img.setImageResource(R.drawable.vip1);
                    txtSlot.setTypeface(Typeface.DEFAULT);
                img.setImageResource(R.drawable.vip1);
                txtSave.setVisibility(View.GONE);
                break;
            case 1:
                txtSlot.setText(R.string.vip2_title);

                if (current_Purchased == position) {
                    txtSlot.setTypeface(Typeface.DEFAULT_BOLD);
                    root.setBackgroundResource(R.drawable.layout_bg);

                } else if (current_Purchased != -1)
                    txtSlot.setTypeface(Typeface.DEFAULT);
                img.setImageResource(R.drawable.vip2);
                txtSave.setText(R.string.vip2_sub);
                break;
            case 2:
                txtSlot.setText(R.string.vip3_title);

                if (current_Purchased == position) {
                    txtSlot.setTypeface(Typeface.DEFAULT_BOLD);
                    root.setBackgroundResource(R.drawable.layout_bg);

                } else if (current_Purchased != -1)
                    txtSlot.setTypeface(Typeface.DEFAULT);
                img.setImageResource(R.drawable.vip3);
                txtSave.setSelected(true);
                txtSave.setText(R.string.vip3_sub);
                break;
        }


        return row;
    }
}
