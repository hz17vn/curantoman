package com.cryptobuddy.ryanbridges.cryptobuddy.app.event.presenter;

import com.cryptobuddy.ryanbridges.cryptobuddy.app.BasePresenter;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.view.ListEventView;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.EventInteractor;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.OnResponseListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.AppHelper;

import java.util.List;

public class ListEventPresenter extends BasePresenter<ListEventView> {
    private final EventInteractor mInteractor;

    public ListEventPresenter(EventInteractor eventInteractor) {
        mInteractor = eventInteractor;
    }

    public void loadEvents(String authorization, int page) {
        mInteractor.loadEvents(authorization, page, new OnResponseListener<List<EventItem>>() {
            @Override
            public void onSuccess(List<EventItem> data) {
                ListEventView view = getView();
                if(view == null) {
                    return;
                }
                view.showEvents(data);
            }

            @Override
            public void onFailure(ErrorType error) {
                if(getView() == null) {
                    return;
                }
                getView().onShowEventsFailure(error);
            }
        });
    }
}
