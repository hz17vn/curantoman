package com.cryptobuddy.ryanbridges.cryptobuddy.app;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.cryptobuddy.ryanbridges.cryptobuddy.helper.Language;

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(Language.onAttach(newBase));
    }
}
