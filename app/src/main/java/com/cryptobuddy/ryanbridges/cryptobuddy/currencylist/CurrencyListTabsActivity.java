package com.cryptobuddy.ryanbridges.cryptobuddy.currencylist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.BaseActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.ToolbarViewListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.setting.SettingActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.AcquireFragment;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.BillingManager;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.BillingProvider;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.Gold3MonthsDelegate;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.Gold6MonthsDelegate;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.GoldMonthlyDelegate;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.MainViewController;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.SkuRowData;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.SkusAdapter;
import com.cryptobuddy.ryanbridges.cryptobuddy.models.rest.CMCCoin;
import com.cryptobuddy.ryanbridges.cryptobuddy.news.NewsListActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.views.BuyAdapter;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.content.ContentValues.TAG;
import static com.cryptobuddy.ryanbridges.cryptobuddy.billing.BillingManager.BILLING_MANAGER_NOT_INITIALIZED;

/**
 * Created by Ryan on 1/21/2018.
 */

public class CurrencyListTabsActivity extends BaseActivity implements ViewPager.OnPageChangeListener,
        FavoriteCurrencyListFragment.AllCoinsListUpdater, AllCurrencyListFragment.FavoritesListUpdater, BillingProvider, ToolbarViewListener,
        View.OnClickListener {

    private AcquireFragment mAcquireFragment;

    private SectionsPagerAdapterCurrencyList mSectionsPagerAdapter;
    public ViewPager mViewPager;
    boolean doubleBackToExitPressedOnce = false;
    public static String IMAGE_URL_FORMAT = "https://s2.coinmarketcap.com/static/img/coins/32x32/%s.png";
    public final static String DAY = "24h";
    public final static String WEEK = "7d";
    public final static String HOUR = "1h";
    public final static String SORT_SETTING = "sort_setting";
    public AppCompatActivity context;
    private BillingManager mBillingManager;
    private static final String DIALOG_TAG = "dialog";
    private MainViewController mViewController;
    private View mLoadingView;

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_list_tabs);
        context = this;
        mLoadingView = findViewById(R.id.screen_wait);
        Toolbar toolbar = findViewById(R.id.toolbar_event_details);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.baseline_menu_white_24);
        }
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mNavigationView = findViewById(R.id.nav_view);
        TabLayout tabLayout = findViewById(R.id.materialTabHost);
        mViewPager = findViewById(R.id.currency_list_tabs_container);
        if (savedInstanceState != null) {
            mAcquireFragment = (AcquireFragment) getSupportFragmentManager()
                    .findFragmentByTag(DIALOG_TAG);
        }
        mViewController = new MainViewController(this);

        mBillingManager = new BillingManager(this, this, mViewController.getUpdateListener());

        mSectionsPagerAdapter = new SectionsPagerAdapterCurrencyList(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(mViewPager);
        initMenu();
        Intent intent = getIntent();
        if (intent != null && intent.getBooleanExtra("show_purchase", false)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showVipSubscribe();

                }
            }, 1000);
        }
        Log.d("Firebase", "token "+ FirebaseInstanceId.getInstance().getToken());
    }

    private void initMenu() {
        findViewById(R.id.menu_news).setOnClickListener(this);
        findViewById(R.id.menu_vip_subscribe).setOnClickListener(this);
        findViewById(R.id.menu_setting).setOnClickListener(this);
        findViewById(R.id.menu_bitmex).setOnClickListener(this);
        findViewById(R.id.menu_facebook).setOnClickListener(this);
        findViewById(R.id.menu_tele).setOnClickListener(this);
        findViewById(R.id.menu_share).setOnClickListener(this);
    }

    public boolean isAcquireFragmentShown() {
        return mAcquireFragment != null && mAcquireFragment.isVisible();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    private void setWaitScreen(boolean set) {
        mLoadingView.setVisibility(set ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageSelected(int position) {

        Fragment fragment = mSectionsPagerAdapter.getFragment(position);
        if (fragment != null) {
            fragment.onResume();
        }
        if (mBillingManager != null
                && mBillingManager.getBillingClientResponseCode() == BillingClient.BillingResponse.OK) {
            mBillingManager.queryPurchases();
        }
    }

    private void showVipSubscribe() {
        if (mBillingManager != null
                && mBillingManager.getBillingClientResponseCode()
                > BILLING_MANAGER_NOT_INITIALIZED) {
            querySkuDetails();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tap back again to exit.", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    public void removeFavorite(CMCCoin coin) {
        FavoriteCurrencyListFragment frag = (FavoriteCurrencyListFragment) mSectionsPagerAdapter.getFavoriteCoinsFragment();
        if (frag != null) {
            frag.removeFavorite(coin);
        }
    }

    public void addFavorite(CMCCoin coin) {
        FavoriteCurrencyListFragment frag = (FavoriteCurrencyListFragment) mSectionsPagerAdapter.getFavoriteCoinsFragment();
        if (frag != null) {
            frag.addFavorite(coin);
        }
    }

    public void allCoinsModifyFavorites(CMCCoin coin) {
        AllCurrencyListFragment frag = (AllCurrencyListFragment) mSectionsPagerAdapter.getAllCoinsFragment();
        if (frag != null) {
            frag.getAdapter().notifyDataSetChanged();
        }
    }

    public void performFavsSort() {
        FavoriteCurrencyListFragment frag = (FavoriteCurrencyListFragment) mSectionsPagerAdapter.getFavoriteCoinsFragment();
        if (frag != null) {
            frag.performFavsSort();
        }
    }

    /**
     * Show an alert dialog to the user
     *
     * @param messageId String id to display inside the alert dialog
     */
    @UiThread
    public void alert(@StringRes int messageId) {
        alert(messageId, null);
    }

    /**
     * Show an alert dialog to the user
     *
     * @param messageId     String id to display inside the alert dialog
     * @param optionalParam Optional attribute for the string
     */
    @UiThread
    public void alert(@StringRes int messageId, @Nullable Object optionalParam) {
        if (Looper.getMainLooper().getThread() != Thread.currentThread()) {
            throw new RuntimeException("Dialog could be shown only from the main thread");
        }

        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setNeutralButton("OK", null);

        if (optionalParam == null) {
            bld.setMessage(messageId);
        } else {
            bld.setMessage(getResources().getString(messageId, optionalParam));
        }

        bld.create().show();
    }

    @Override
    public void onDestroy() {
        if (mBillingManager != null) {
            mBillingManager.destroy();
        }
        super.onDestroy();
    }

    public void performAllCoinsSort() {
        AllCurrencyListFragment frag = (AllCurrencyListFragment) mSectionsPagerAdapter.getAllCoinsFragment();
        if (frag != null) {
            frag.performAllCoinsSort();
        }
    }

    public void showRefreshedUi() {
        //   setWaitScreen(false);
        //  updateUi();
      /*  if (mAcquireFragment != null) {
            mAcquireFragment.refreshUI();
        }*/

        if (currentDialog != null) {
            try {
                currentDialog.dismiss();
            } catch (Exception e) {

            }
        }

    }

    public void onBillingManagerSetupFinished() {
        if (mAcquireFragment != null) {
            mAcquireFragment.onManagerReady(this);
        }
    }

    @Override
    public BillingManager getBillingManager() {
        return mBillingManager;
    }

    @Override
    public boolean isPremiumPurchased() {
        return mViewController.isPremiumPurchased();
    }

    @Override
    public boolean isGoldMonthlySubscribed() {
        return mViewController.isGoldMonthlySubscribed();
    }

    @Override
    public boolean isGold3MonthsSubscribed() {
        return mViewController.isGold3MonthsSubscribed();
    }

    @Override
    public boolean isGold6MonthSubscribed() {
        return mViewController.isGold6MonthsSubscribed();
    }

    @Override
    public boolean isGoldYearlySubscribed() {
        return mViewController.isGoldYearlySubscribed();
    }

    @Override
    public boolean isTankFull() {
        return false;
    }

    public DialogFragment getDialogFragment() {
        return mAcquireFragment;
    }

    @Override
    public String getCurrentSKU() {
        if (isGold6MonthSubscribed()) return Gold6MonthsDelegate.SKU_ID;
        if (isGold3MonthsSubscribed()) return Gold3MonthsDelegate.SKU_ID;
        if (isGoldMonthlySubscribed()) return GoldMonthlyDelegate.SKU_ID;
        return null;
    }

    private void openSetting() {
        Intent intent = new Intent(this, SettingActivity.class);
        startActivity(intent);
    }

    @Override
    public void changeToolbarTitle(String title) {
        setTitle(title);
    }

    DialogInterface currentDialog;

    public void showBuyDialog(final List<SkuRowData> inList) {
        setWaitScreen(false);
        String purchased_SDK = getCurrentSKU();
        int purchased_index = -1;

        for (int i = 0; i < inList.size(); i++)
            if (inList.get(i).getSku().equals(purchased_SDK)) {
                purchased_index = i;
                break;
            }
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(new ContextThemeWrapper(CurrencyListTabsActivity.this, R.style.appstyle0));

        LayoutInflater inflater = getLayoutInflater();

        View titleView = inflater.inflate(R.layout.dialog_title, null);
        builderSingle.setCustomTitle(titleView);

        TextView txtTitle = titleView.findViewById(R.id.txtTitleDialog);


        if (purchased_index == -1)
            txtTitle.setText(R.string.vip_title_dialog);
        else txtTitle.setText(R.string.button_purchase_change);


        //  builderSingle.setMessage(R.string.access_to_vip_event);
        BuyAdapter buyAdapter = new BuyAdapter(CurrencyListTabsActivity.this, inList, purchased_index);
        builderSingle.setAdapter(buyAdapter, null);
        AlertDialog alertDialogObject = builderSingle.create();
        ListView listView = alertDialogObject.getListView();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                SkuRowData data = inList.get(position);
                if (data != null) {
                    if (getCurrentSKU() != null && !getCurrentSKU().equals(data.getSku())) {
                        // If we already subscribed to yearly gas, launch replace flow
                        ArrayList<String> currentSubscriptionSku = new ArrayList<>();
                        currentSubscriptionSku.add(getCurrentSKU());
                        getBillingManager().initiatePurchaseFlow(data.getSku(),
                                currentSubscriptionSku, data.getSkuType());
                    } else {
                        getBillingManager().initiatePurchaseFlow(data.getSku(),
                                data.getSkuType());
                    }
                }
            }
        });
        listView.setDividerHeight(150); // set height
        listView.setDivider(new ColorDrawable(Color.DKGRAY)); // set color
        alertDialogObject.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                currentDialog = dialogInterface;
            }
        });
        alertDialogObject.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                currentDialog = null;

            }
        });
        alertDialogObject.show();
        buyAdapter.notifyDataSetChanged();
    }

    public void querySkuDetails() {
        setWaitScreen(true);
        long startTime = System.currentTimeMillis();
        Log.d(TAG, "querySkuDetails() got subscriptions and inApp SKU details lists for: "
                + (System.currentTimeMillis() - startTime) + "ms");
        List<String> subscriptionsSkus = getSkuList();
        getBillingManager().querySkuDetailsAsync(BillingClient.SkuType.SUBS, subscriptionsSkus, new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {

                List<SkuRowData> inList = new ArrayList<>();
                if (responseCode != BillingClient.BillingResponse.OK) {
                    Log.w(TAG, "Unsuccessful query for type: " + BillingClient.SkuType.SUBS
                            + ". Error code: " + responseCode);
                } else if (skuDetailsList != null
                        && skuDetailsList.size() > 0) {

                    for (int i = 0; i < skuDetailsList.size(); i++)
                        for (int j = i + 1; j < skuDetailsList.size(); j++)
                            if (skuDetailsList.get(i).getPriceAmountMicros() > skuDetailsList.get(j).getPriceAmountMicros()) {
                                Collections.swap(skuDetailsList, i, j);
                            }

                    // Then fill all the other rows
                    for (SkuDetails details : skuDetailsList) {
                        Log.i(TAG, "Adding sku: " + details);

                        inList.add(new SkuRowData(details, SkusAdapter.TYPE_NORMAL,
                                BillingClient.SkuType.SUBS));
                    }
                    showBuyDialog(inList);
                }

            }
        });

    }

    /**
     * Returns the list of all SKUs for the billing type specified
     */
    public final List<String> getSkuList() {
        List<String> result = new ArrayList<>();
        result.add(GoldMonthlyDelegate.SKU_ID);
        result.add(Gold3MonthsDelegate.SKU_ID);
        result.add(Gold6MonthsDelegate.SKU_ID);
        return result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_setting:
                openSetting();
                mDrawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.menu_news:
                startActivity(new Intent(context, NewsListActivity.class));
                mDrawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.menu_vip_subscribe:
                showVipSubscribe();
                mDrawerLayout.closeDrawer(Gravity.START);
                break;
            case R.id.menu_facebook:
                try {
                    getPackageManager()
                            .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("fb://profile/100022901151141"))); //Trys to make intent with FB's URI
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.facebook.com/100022901151141"))); //catches and opens a url to the desired page
                }
                break;
            case R.id.menu_tele:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/marginBTC"));
                startActivity(intent);
                break;
            case R.id.menu_bitmex:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.bitmex.com/register/TXLq9E"));
                startActivity(i);
                break;

            case R.id.menu_share:
                String shareBody = "https://play.google.com/store/apps/details?id=com.master.curantoman.crypto";
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "\n\n");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.app_name)));

                break;
        }
    }
}
