package com.cryptobuddy.ryanbridges.cryptobuddy.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class CheckedManager extends SQLiteOpenHelper {

    private static CheckedManager sInstance;

    private static final String DATABASE_NAME = "CheckedItem.db";
    private static final String DATABASE_TABLE = "ITEMS";
    private static final int DATABASE_VERSION = 1;
    private static final String COL_0 = "ID";
    private static final String COL_1 = "ITEM_KEY";
    private static final String COL_2 = "ITEM_TYPE";

    // Use singleton design pattern so there is only ever one DB object floating around
    public static synchronized CheckedManager getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new CheckedManager(context.getApplicationContext());
        }
        return sInstance;
    }

    private CheckedManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Drop the table if it exists
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        // Create the new table with 2 columns. One for ID and one for the coin list
        db.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" + COL_0 + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_1 + " TEXT, " + COL_2 + " TEXT)");
    }

    public List<String> getItems(String type) {
        List<String> data = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COL_1 + " FROM " + DATABASE_TABLE + " WHERE " + COL_2 + " = ?", new String[]{type});
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String item = cursor.getString(cursor.getColumnIndex(COL_1));
                data.add(item);
            } while (cursor.moveToFirst());
        }
        cursor.close();
        return data;
    }

    public void addItem(String key, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COL_1 + " FROM " + DATABASE_TABLE + " WHERE " + COL_1 + " = ? AND " + COL_2 + " = ?", new String[]{key, type});
        boolean exist = cursor.getCount() > 0;
        cursor.close();
        if(exist) {
            return;
        }
        ContentValues data = new ContentValues();
        data.put(COL_1, key);
        data.put(COL_2, type);
        db.insert(DATABASE_TABLE, null, data);
    }

    public boolean isExist(String key, String type) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT " + COL_1 + " FROM " + DATABASE_TABLE + " WHERE " + COL_1 + " = ? AND " + COL_2 + " = ?", new String[]{key, type});
        boolean exist = cursor.getCount() > 0;
        cursor.close();
        return exist;
    }

    public boolean removeItem(String key, String type) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(DATABASE_TABLE, COL_1 + " = " + key + " AND " + COL_2 + " = " + type, new String[]{key, type}) > 0;
    }
}
