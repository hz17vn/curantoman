package com.cryptobuddy.ryanbridges.cryptobuddy.app;

public interface ToolbarViewListener {
    void changeToolbarTitle(String title);
}