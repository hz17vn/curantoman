package com.cryptobuddy.ryanbridges.cryptobuddy.app.event;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.presenter.EventDetailPresenter;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.view.EventDetailView;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.app.AppConfig;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.EventInteractor;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.AppHelper;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.CheckedManager;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.MCrypt;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.ViewHelper;

import java.util.Date;

public class EventDetailActivity extends AppCompatActivity implements EventDetailView, View.OnClickListener {
    private TextView mTvTitle;
    private TextView mTvDescription;
    private WebView mWvContent;
    private TextView mTvCreatedTime;
    private ImageView mImgCoinType;
    private EventDetailPresenter mPresenter;
    private int mEventId;
    private FloatingActionButton imgType;

    private MCrypt mCrypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail_scrollable_actvity);
        setupView();
        Intent intent = getIntent();
        if (intent != null) {
            mEventId = intent.getIntExtra("id", 0);
        }
        mPresenter = new EventDetailPresenter(new EventInteractor());
        mPresenter.attachView(this);
        String authorization = AppConfig.getInstance().getAuthentication(getApplicationContext());
        mPresenter.loadEvent(authorization, mEventId);
        String key = AppHelper.getContryCode(getApplicationContext());
        mCrypt = new MCrypt(key);
    }

    private void setupView() {
        Toolbar toolbar = findViewById(R.id.toolbar_event_details);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        mTvTitle = findViewById(R.id.tv_title);
        mTvDescription = findViewById(R.id.tv_description);
        mWvContent = findViewById(R.id.wv_news_detail_content);
        mTvCreatedTime = findViewById(R.id.tv_created_time);
        mWvContent.setWebChromeClient(new VideoWebChromeClient());
        mWvContent.setFocusable(false);
        mImgCoinType = findViewById(R.id.img_coin_type);
        ViewHelper.improveWebSetting(mWvContent);
        findViewById(R.id.menu_tele).setOnClickListener(this);
        findViewById(R.id.menu_facebook).setOnClickListener(this);
        findViewById(R.id.menu_bitmex).setOnClickListener(this);
        imgType = findViewById(R.id.fab);
    }

    @Override
    public void showEvents(EventItem data) {
        if (data == null) {
            Log.d("AAAA", "empty data");
            return;
        }
        CheckedManager.getInstance(getApplicationContext()).addItem(String.valueOf(data.getId()), EventItem.TYPE);
        mTvTitle.setText(decryptData(data.getTitle()));
        mTvDescription.setText(decryptData(data.getDescription()));
        if (!TextUtils.isEmpty(data.getContent())) {
            mWvContent.setVisibility(View.VISIBLE);
            mWvContent.loadData(decryptData(data.getContent()), "text/html; charset=utf-8", "utf-8");
        } else {
            mWvContent.setVisibility(View.GONE);
        }
        mTvCreatedTime.setText(ViewHelper.formatDateTime(new Date(data.getCreatedAt())));
        if (EventItem.COIN_TYPE_BIT.equalsIgnoreCase(data.getEventType())) {
            mImgCoinType.setImageResource(R.drawable.bitgold);
        } else {
            mImgCoinType.setImageResource(R.drawable.altcoin);
        }

        if ("free".equals(data.getUser_type())){
            imgType.setImageResource(R.drawable.free);
        }else imgType.setImageResource(R.drawable.vip_ticket   );
    }

    @Override
    public void onShowEventsFailure(ErrorType error) {
        if (error == null) {
            return;
        }
        if (error.getStatus() == 403 || error.getStatus() == 401) {
            AppHelper.loadAccessToken(getApplicationContext());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    private class VideoWebChromeClient extends WebChromeClient {
        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        VideoWebChromeClient() {
        }

        public Bitmap getDefaultVideoPoster() {
            return BitmapFactory.decodeResource(EventDetailActivity.this.getApplicationContext().getResources(), R.id.frame_full_screen);
        }

        @Override
        public void onHideCustomView() {
            ((FrameLayout) EventDetailActivity.this.getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            EventDetailActivity.this.getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            EventDetailActivity.this.setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
        }

        @Override
        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback) {
            if (this.mCustomView != null) {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = EventDetailActivity.this.getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = EventDetailActivity.this.getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout) EventDetailActivity.this.getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            int newUiOptions = EventDetailActivity.this.getWindow().getDecorView().getSystemUiVisibility();
            if (Build.VERSION.SDK_INT >= 19) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
            } else if(Build.VERSION.SDK_INT >= 16) {
                newUiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;
            } else {
                newUiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
            }
            EventDetailActivity.this.getWindow().getDecorView().setSystemUiVisibility(newUiOptions);// 3846);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_facebook:
                try {
                    getPackageManager()
                            .getPackageInfo("com.facebook.katana", 0); //Checks if FB is even installed.
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("fb://profile/100022901151141"))); //Trys to make intent with FB's URI
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://www.facebook.com/100022901151141"))); //catches and opens a url to the desired page
                }
                break;
            case R.id.menu_tele:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/marginBTC"));
                startActivity(intent);
                break;
            case R.id.menu_bitmex:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.bitmex.com/register/TXLq9E"));
                startActivity(i);
                break;
        }
    }

    private String decryptData(String txt) {
        byte[] base64 = Base64.decode(txt, Base64.CRLF);
        String[] item = (new String(base64)).split("::");
        if(item.length < 2) {
            return "";
        }
        byte[] iv = MCrypt.hexToBytes(item[1]);
        base64 = Base64.decode(item[0], Base64.CRLF);
        try {
            return mCrypt.decrypt(base64, iv);
        } catch (Exception e) {
            return "";
        }
    }
}
