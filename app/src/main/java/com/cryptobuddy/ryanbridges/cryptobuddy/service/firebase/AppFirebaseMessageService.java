package com.cryptobuddy.ryanbridges.cryptobuddy.service.firebase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.util.Log;

import com.cryptobuddy.ryanbridges.cryptobuddy.billing.Gold3MonthsDelegate;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.Gold6MonthsDelegate;
import com.cryptobuddy.ryanbridges.cryptobuddy.billing.GoldMonthlyDelegate;
import com.cryptobuddy.ryanbridges.cryptobuddy.service.billing.BillingService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class AppFirebaseMessageService extends FirebaseMessagingService {


    @Override
    public void onCreate() {

        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
      //  Log.d("AAAA", "onMessageReceived: " + remoteMessage.getData());
        if (remoteMessage.getData() == null) {
            return;
        }
        Map<String, String> data = remoteMessage.getData();
        if (data.get("user_type") != null && data.get("user_type").equals("paid")) {
            Intent intent = new Intent(this, BillingService.class);
            HashMap<String, String> copyMap = new HashMap<String, String>(data);
            intent.putExtra("map", copyMap);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, intent);
            }else
             startService(intent);
        } else
            pushNotify(remoteMessage);
    }

    private void pushNotify(RemoteMessage remoteMessage) {
        NotificationProvider notificationProvider = NotificationFactory.create(getApplicationContext(), remoteMessage.getData(), false);
        if (notificationProvider != null) {
            notificationProvider.pushNotify();
        }

    }
}
