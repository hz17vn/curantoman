package com.cryptobuddy.ryanbridges.cryptobuddy.data;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.EventRestApi;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.AccessToken;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.DataWrap;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;

import java.util.List;

import retrofit2.Call;

public class RestfulService {
    private static RestfulService sInstance;
    private EventRestApi mEventRestApi;

    private RestfulService() {
        mEventRestApi = EventRestApi.Factory.create();
    }

    private void changeRestLocale() {
        mEventRestApi = EventRestApi.Factory.create();
    }

    public Call<List<EventItem>> loadEvents(String authorization, int page) {
        return mEventRestApi.loadEvents(authorization, page);
    }

    public Call<DataWrap<EventItem>> loadEvent(String authorization, int eventId) {
        return mEventRestApi.loadEvent(authorization, eventId);
    }

    public Call<AccessToken> loadAccessToken(String deviceTokenId) {
        return mEventRestApi.loadAccessToken(deviceTokenId);
    }

    public static RestfulService getInstance() {
        if(sInstance == null) {
            sInstance = new RestfulService();
        }
        return sInstance;
    }

    public static void changeLocale() {
        RestfulService.getInstance().changeRestLocale();
    }
}
