package com.cryptobuddy.ryanbridges.cryptobuddy.app.event;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.presenter.ListEventPresenter;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.view.ListEventView;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.widget.EndlessRecyclerViewScrollListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.currencylist.CurrencyListTabsActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.app.AppConfig;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.EventInteractor;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.AppHelper;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.MCrypt;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.RecycleItemClickSupport;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.SimpleDividerItemDecoration;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.ViewHelper;

import java.util.List;

public class ListEventFragment extends Fragment implements ListEventView, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout mRefreshLayout;
    private EventRecycleAdapter mAdapter;
    private ListEventPresenter mListEventPresenter;

    private int mCurrentPage = 1;
    private EndlessRecyclerViewScrollListener mOnLoadMoreListener;

    private MCrypt mCrypt;

    public static ListEventFragment newInstance() {
        return new ListEventFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new EventRecycleAdapter(getActivity());
        mListEventPresenter = new ListEventPresenter(new EventInteractor());
        mListEventPresenter.attachView(this);
        loadEvents(1);
        String key = AppHelper.getContryCode(getContext());
        mCrypt = new MCrypt(key);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_event, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.primary));
        RecyclerView listViewEvent = view.findViewById(R.id.lv_event);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listViewEvent.addItemDecoration(new SimpleDividerItemDecoration(getContext()));

        listViewEvent.setLayoutManager(layoutManager);
        listViewEvent.setAdapter(mAdapter);
        RecycleItemClickSupport.addTo(listViewEvent).setOnItemClickListener(new RecycleItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                EventItem item = mAdapter.getItem(position);
                if (item == null) {
                    return;
                }
                Boolean isVIP = ((CurrencyListTabsActivity) getActivity()).getCurrentSKU() != null;
                if (!isVIP && "paid".equals(item.getUser_type())) {
                    ((CurrencyListTabsActivity) getActivity()).querySkuDetails();
                } else
                    ViewHelper.displayEventDetail(getContext(), item.getId());
            }
        });
        // Paging
        mOnLoadMoreListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page) {
                mCurrentPage++;
                loadEvents(mCurrentPage);
            }
        };
        listViewEvent.addOnScrollListener(mOnLoadMoreListener);
    }

    private void loadEvents(int page) {
        showLoading(true);
        String authorization = AppConfig.getInstance().getAuthentication(getContext());
        mListEventPresenter.loadEvents(authorization, page);
    }

    @Override
    public void showEvents(List<EventItem> data) {
        showLoading(false);
        if (data == null) {
            return;
        }
        for(EventItem item : data) {
            item.setTitle(decryptData(item.getTitle()));
            item.setDescription(decryptData(item.getDescription()));
        }
        if (mCurrentPage == 1) {
            mAdapter.updateData(data);
        } else {
            mAdapter.addData(data);
        }
    }

    @Override
    public void onShowEventsFailure(ErrorType error) {
        showLoading(false);
        if (error == null) {
            return;
        }
        if (error.getStatus() == 403 || error.getStatus() == 401) {
            AppHelper.loadAccessToken(getContext());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mListEventPresenter.detachView();
    }

    @Override
    public void onRefresh() {
        loadEvents(1);
        mOnLoadMoreListener.resetState();
        mCurrentPage = 1;
    }

    private void showLoading(boolean isLoading) {
        if (getView() != null) {
            mRefreshLayout.setRefreshing(isLoading);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.notifyDataSetChanged();
    }

    private String decryptData(String txt) {
        byte[] base64 = Base64.decode(txt, Base64.CRLF);
        String[] item = (new String(base64)).split("::");
        if(item.length < 2) {
            return "";
        }
        byte[] iv = MCrypt.hexToBytes(item[1]);
        base64 = Base64.decode(item[0], Base64.CRLF);
        try {
            return mCrypt.decrypt(base64, iv);
        } catch (Exception e) {
            return "";
        }
    }
}
