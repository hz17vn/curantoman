package com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;

public interface OnResponseListener<T> {
    void onSuccess(T data);

    void onFailure(ErrorType error);
}
