package com.cryptobuddy.ryanbridges.cryptobuddy.app.setting;

import android.content.Context;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;
import android.view.View;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.ToolbarViewListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.AppHelper;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.Language;


public class SettingsFragment extends PreferenceFragment {
    private ToolbarViewListener mToolBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateTitle();
        initSetting();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ToolbarViewListener) {
            mToolBar = (ToolbarViewListener) context;
        }
    }

    private void updateTitle() {
        if (mToolBar != null) {
            mToolBar.changeToolbarTitle(getResources().getString(R.string.menu_item_setting));
        }
    }

    private void initSetting() {
        // Language
        ListPreference langPref = (ListPreference) findPreference("language_setting");
        String lang = Language.getLanguage(getActivity());
        langPref.setValue(lang);
        langPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Language.setLocale(getActivity(), o.toString());
                AppHelper.changeLanguage(getActivity());
                AppHelper.restartApp(getActivity());
                return true;
            }
        });
    }
}