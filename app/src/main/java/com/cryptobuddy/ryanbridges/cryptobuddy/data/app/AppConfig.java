package com.cryptobuddy.ryanbridges.cryptobuddy.data.app;

import android.content.Context;
import android.content.SharedPreferences;

public class AppConfig {
    private static final String PREF_APP_CONFIG = "app_config";
    private static final String KEY_ACCESS_TOKEN = "access_token";

    private static AppConfig sInstance = null;

    private AppConfig() {

    }

    public static AppConfig getInstance() {
        if (sInstance == null) {
            sInstance = new AppConfig();
        }
        return sInstance;
    }

    public String getAuthentication(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_APP_CONFIG, Context.MODE_PRIVATE);
        return "Bearer "+prefs.getString(KEY_ACCESS_TOKEN, "");
    }

    public void saveAccessToken(Context context, String accessToken) {
        SharedPreferences prefs = context.getSharedPreferences(PREF_APP_CONFIG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(KEY_ACCESS_TOKEN, accessToken);
        editor.apply();
    }
}
