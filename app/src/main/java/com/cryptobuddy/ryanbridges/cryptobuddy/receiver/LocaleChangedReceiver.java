package com.cryptobuddy.ryanbridges.cryptobuddy.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.RestfulService;

public class LocaleChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_LOCALE_CHANGED.equals(action)) {
            RestfulService.changeLocale();
        }

    }
}