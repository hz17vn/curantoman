package com.cryptobuddy.ryanbridges.cryptobuddy.service.firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.cryptobuddy.ryanbridges.cryptobuddy.R;
import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.EventDetailActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.currencylist.CurrencyListTabsActivity;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.Language;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.util;

import java.lang.reflect.Array;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class NotificationFactory {


    private static NotificationProvider createNewsNotification(Context context, Map<String, String> data, boolean isHide) {

        int id;
        try {
            id = Integer.parseInt(data.get("id"));
        } catch (NumberFormatException e) {
            id = 0;
        }
        String title = data.get("title_" + Language.getDefaultLanguage());
        Set<String> data2 =  data.keySet();

        NotificationProvider notification = new NotificationProvider(context);
        notification.putExtra("id", id);

        if (isHide){
            title = context.getResources().getString(R.string.title_free);
            notification.setText(context.getResources().getString(R.string.content_free));
            notification.setClass(CurrencyListTabsActivity.class);
            notification.putExtra("show_purchase", true);

        }else {
            notification.setClass(EventDetailActivity.class);
        }
        notification.setTitle(title);
        notification.setId(id);

        return notification;
    }



    public static NotificationProvider create(Context context, Map<String, String> data, boolean isHideNoti) {
        String type = data.get("type");
        if ("event".equals(type)) {
            return createNewsNotification(context, data, isHideNoti);
        }
        return null;
    }


}
