package com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model;

public class ErrorType {
    private String message;
    private int status = 0;

    public ErrorType(String message) {
        this.message = message;
    }

    public ErrorType(int status, String message) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public int getStatus() {
        return status;
    }
}
