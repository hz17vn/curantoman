package com.cryptobuddy.ryanbridges.cryptobuddy.currencylist;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.cryptobuddy.ryanbridges.cryptobuddy.app.event.ListEventFragment;

public class SectionsPagerAdapterCurrencyList extends FragmentPagerAdapter {

    private SparseArray<String> mFragmentTags;
    private FragmentManager mFragmentManager;

    protected SectionsPagerAdapterCurrencyList(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
        mFragmentTags = new SparseArray<>();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object object = super.instantiateItem(container, position);
        if (object instanceof Fragment) {
            Fragment fragment = (Fragment) object;
            String tag = fragment.getTag();
            mFragmentTags.put(position, tag);
        }
        return object;
    }

    public Fragment getFragment(int position) {
        Fragment fragment = null;
        String tag = mFragmentTags.get(position);
        if (tag != null) {
            fragment = mFragmentManager.findFragmentByTag(tag);
        }
        return fragment;
    }

    public Fragment getAllCoinsFragment() {
        return getFragment(1);
    }

    public Fragment getFavoriteCoinsFragment() {
        return getFragment(2);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ListEventFragment.newInstance();
            case 1:
                return AllCurrencyListFragment.newInstance();
            case 2:
                return FavoriteCurrencyListFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        // Total pages to show
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Events";
            case 1:
                return "All Coins";
            case 2:
                return "Favorites";
            default:
                return null;
        }
    }
}
