package com.cryptobuddy.ryanbridges.cryptobuddy.service.firebase;

import com.cryptobuddy.ryanbridges.cryptobuddy.helper.AppHelper;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

public class AppFirebaseInstanceIdService extends FirebaseInstanceIdService {
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        FirebaseMessaging.getInstance().subscribeToTopic("event");
        AppHelper.loadAccessToken(getApplicationContext());
    }
}
