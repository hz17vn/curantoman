package com.cryptobuddy.ryanbridges.cryptobuddy.service.app;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.RestfulService;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.app.AppConfig;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.EventInteractor;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.interactor.OnResponseListener;
import com.cryptobuddy.ryanbridges.cryptobuddy.helper.AppHelper;
import com.google.firebase.iid.FirebaseInstanceId;

public class AppService extends IntentService {
    public static final String ACTION_LOAD_ACCESS_TOKEN = "ACTION_LOAD_ACCESS_TOKEN";
    public static final String ACTION_CHANGE_LANGUAGE = "ACTION_CHANGE_LANGUAGE";

    public AppService() {
        super("AppService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent == null) {
            return;
        }
        String action = intent.getAction();
        if(ACTION_LOAD_ACCESS_TOKEN.equals(action)) {
            loadAccessToken();
            return;
        }
        if(ACTION_CHANGE_LANGUAGE.equals(action)) {
            changeLanguage();
        }
    }

    private void loadAccessToken() {
        EventInteractor interactor = new EventInteractor();
        if(FirebaseInstanceId.getInstance() == null) {
            return;
        }
        interactor.loadAccessToken(FirebaseInstanceId.getInstance().getToken(), new OnResponseListener<String>() {
            @Override
            public void onSuccess(String data) {
                AppConfig.getInstance().saveAccessToken(getApplicationContext(), data);
                AppHelper.restartApp(getApplicationContext());
            }

            @Override
            public void onFailure(ErrorType error) {
            }
        });
    }

    private void changeLanguage() {
        RestfulService.changeLocale();
    }
}
