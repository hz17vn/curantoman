package com.cryptobuddy.ryanbridges.cryptobuddy.app;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.cryptobuddy.ryanbridges.cryptobuddy.helper.Language;

public class MainApplication extends MultiDexApplication {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(Language.onAttach(base));
    }
}
