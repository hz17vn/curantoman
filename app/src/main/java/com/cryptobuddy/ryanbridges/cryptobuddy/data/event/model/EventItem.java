package com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model;


public class EventItem {
    public static final String TYPE = "event";
    public static final String COIN_TYPE_BIT = "bitcoin";
    public static final String COIN_TYPE_ALT = "altcoin";

    private int id;
    private String title;
    private String description;
    private String content;
    private long created_at;
    private String event_type;
    private String user_type ;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public long getCreatedAt() {
        return created_at*1000;
    }

    public String getEventType() {
        return event_type;
    }

    public String getContent() {
        return content;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
