package com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model;

public class DataWrap<T> {
    private T data;

    public T getData() {
        return data;
    }
}
