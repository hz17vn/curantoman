package com.cryptobuddy.ryanbridges.cryptobuddy.app.event.view;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;

import java.util.List;

public interface ListEventView {
    void showEvents(List<EventItem> data);

    void onShowEventsFailure(ErrorType error);
}
