package com.cryptobuddy.ryanbridges.cryptobuddy.app.event.view;

import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.ErrorType;
import com.cryptobuddy.ryanbridges.cryptobuddy.data.event.model.EventItem;

public interface EventDetailView {
    void showEvents(EventItem data);

    void onShowEventsFailure(ErrorType error);
}
